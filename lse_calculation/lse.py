import numpy as np
from scipy import signal
from math import ceil


def matlab_style_gauss2d(shape=(3, 3), sigma=0.5):
    """
    2D gaussian mask - should give the same result as MATLAB's
    fspecial('gaussian',[shape],[sigma])
    ----------
    :param shape : tuple, optional
        size of the filter. The default is (3,3).
    :param sigma : float, optional
        Standard deviation. The default is 0.5.

    Returns
    -------
    :return: h : numpy array
    2D gaussian filter
    """
    m, n = [(ss - 1.0) / 2.0 for ss in shape]
    y, x = np.ogrid[-m: m + 1, -n: n + 1]
    h = np.exp(-(x * x + y * y) / (2.0 * sigma * sigma))
    h[h < np.finfo(h.dtype).eps * h.max()] = 0
    sumh = h.sum()
    if sumh != 0:
        h /= sumh
    return h


def cutout_zero_diffraction_order(some_spectra, r_0):
    """
    Function cuts the zeroth diffraction order by radius r_0
    Parameters
    ----------
    some_spectra : numpy array complex
        angular spectrum with diffraction orders.
    r_0 : int
        Radius of cutting the 0 diffraction order.
    Returns
    -------
    new_spectrum : complex
        Spectra without the 0 diffraction order of a radius r_0.
    """
    ny, nx = some_spectra.shape
    center_x = nx // 2
    center_y = ny // 2
    res = some_spectra.copy()
    ii = np.arange(ny)[:, np.newaxis]
    jj = np.arange(nx)
    res = np.where(np.sqrt((ii - center_y) ** 2 + (jj - center_x) ** 2) < r_0, 0, res)
    return res


def find_sin_angles_from_hologram(hologram, r_0, pixel_size, wavelength):
    """
    Function returns sinuses of vertical and horizontal components
    of angle between object and reference wave in hologram
    :param hologram: numpy array int
        A hologram as 2D numpy int array
    :param r_0: int
        Radius of cutting the 0 diffraction order
    :param pixel_size: float
        Size of pixel in micrometers
    :param wavelength: float
        Wavelength in micrometers
    :return: vertical_sin: float
        Sinus of vertical component of angle between reference and object wave
    :return: horizontal_sin: float
        Sinus of horizontal component of angle between reference and object wave
    """
    ny, nx = hologram.shape
    spectrum_init = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(hologram)))
    spectrum = abs(spectrum_init)
    new_spectrum = cutout_zero_diffraction_order(spectrum, r_0)
    new_spectrum[: nx // 2][: ny // 2] = 0
    max_intensity_index = np.unravel_index(new_spectrum.argmax(), new_spectrum.shape)
    shift_y = abs(max_intensity_index[0] - ny // 2)
    shift_x = abs(max_intensity_index[1] - nx // 2)
    vertical_sin = shift_y * wavelength / ny / pixel_size
    horizontal_sin = shift_x * wavelength / nx / pixel_size
    return vertical_sin, horizontal_sin


def least_squares(
        hologram,
        r_0,
        pixel_size,
        wavelength,
        window_size,
):
    """
    Function calculates reconstructed complex wave by least-squares estimation method
    (Michael Liebling, Thierry Blu, and Michael Unser, "Complex-wave retrieval from a single off-axis hologram,"
    J. Opt. Soc. Am. A 21, 367-377 (2004))
    with auto determination of angle between object and reference wave
    by calculation of first diffraction order coordinates in initial hologram spectrum

    Parameters
    ----------
    :param hologram : numpy array int
        Intensity hologram to be reconstructed
    :param r_0 : int
        Radius of cutting the 0 diffraction order in pixels
    :param pixel_size: float
        Size of pixel in micrometers
    :param wavelength: float
        Wavelength in micrometers
    :param window_size: int
        Weighting window for a B-spline
    Returns
    ----------
    :return: reconstructed_wave : numpy array complex
        Reconstructed wave
    :return: ref_wave : numpy array complex
        Generated reference wave with same angle between object and reference wave as hologram
    """
    vertical_sin, horizontal_sin = find_sin_angles_from_hologram(hologram, r_0, pixel_size, wavelength)
    ny, nx = hologram.shape
    # print(np.arcsin(vertical_sin)*180/np.pi)
    # print(np.arcsin(horizontal_sin)*180/np.pi)

    ii = np.arange(ny)[:, np.newaxis]
    jj = np.arange(nx)
    ref_wave = np.exp(
        ((ii + 1) * vertical_sin + (jj + 1) * horizontal_sin)
        / wavelength * 1j * 2 * np.pi * pixel_size)

    weight = matlab_style_gauss2d((window_size, window_size), window_size / 6)
    kk = signal.fftconvolve(hologram, weight)
    ll = signal.fftconvolve(hologram * ref_wave, weight)
    m = np.conj(ll)
    a = signal.fftconvolve(weight, ref_wave)
    b = signal.fftconvolve(weight, ref_wave * ref_wave)
    reconstructed_field = np.round(
        (
                ll * np.conj(b)
                + m * (abs(a) * abs(a))
                + kk * np.conj(a)
                - kk * a * np.conj(b)
                - m
                - ll * np.conj(a) * np.conj(a)
        )
        / (
                abs(b) * abs(b)
                + 2 * abs(a) * abs(a)
                - np.conj(b) * a * a
                - 1
                - np.conj(a) * np.conj(a) * b
        ),
        3,
    )
    nx2 = len(reconstructed_field[0])
    ny2 = len(reconstructed_field)
    rounded = ceil(window_size / 2) - 1
    reconstructed_wave = reconstructed_field[
                         rounded: (ny2 - rounded - 1), rounded: (nx2 - rounded - 1)
                         ]
    return reconstructed_wave, ref_wave


def reconstruct_complex_wave_from_holograms(holograms_dict, r_0, pixel_size, wavelength, window):
    """
    Function returns reconstructed complex waves from holograms by LSE method
    :param holograms_dict: dict
        A dictionary of initial holograms, where keys are names of holograms
        or some common part of name in the case of one object hologram set.
        Each element of dictionary contains a list of holograms as 2D int numpy arrays
    :param r_0: int
        Size of zero diffraction order cutout in pixels
    :param wavelength: float
        Wavelength value in micrometers
    :param pixel_size: float
        Camera pixel size in micrometers
    :param window: int
        Weighting window for a B-spline
    :return: rec_waves_dict: dict
        A dictionary of reconstructed complex waves, where keys are names of holograms
        or some common part of name in the case of one object hologram set.
        Each element of dictionary contains a list of reconstructed complex waves as 2D complex numpy arrays.
    :return: ref_lse_waves_dict: dict
        A dictionary of complex reference waves, where keys are names of holograms
        or some common part of name in the case of one object hologram set.
        Each element of dictionary contains a list of complex reference waves,
        generated with same calculated angle between object and reference waves
        as initial holograms as 2D complex numpy arrays.
    """
    rec_waves_dict = {}
    ref_lse_waves_dict = {}
    for key in holograms_dict:
        for holo in holograms_dict[key]:
            rec_u, ref_wave = least_squares(holo, r_0, pixel_size, wavelength, window)
            if key not in rec_waves_dict:
                rec_waves_dict[key] = []
            if key not in ref_lse_waves_dict:
                ref_lse_waves_dict[key] = []
            rec_waves_dict[key].append(rec_u)
            ref_lse_waves_dict[key].append(ref_wave)
    return rec_waves_dict, ref_lse_waves_dict
