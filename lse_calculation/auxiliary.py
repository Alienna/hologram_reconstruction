import os
import numpy as np
from PIL import Image


def create_result_dir(filepath, dir_name):
    """
    Function creates new directory next to given filepath with given name
    Parameters
    ----------
    :param filepath: str
        Path to directory
    :param dir_name: str
        Name of directory to be created
    Returns
    ----------
    :return: result_path: str
        Path to created directory with name dir_name
    """
    result_path = os.path.split(filepath)[0]
    result_path = os.path.join(result_path, dir_name)
    if not os.path.exists(result_path):
        os.mkdir(result_path)
    return result_path


def save_numpy_array(numpy_array, file_name, path_to_result):
    """
    Function saves numpy array as image with given file_name and in given path
    :param numpy_array: numpy array float
        Some array to be normalized by 0-255 and saved as uint8
    :param file_name: str
        Name of image to be saved
    :param path_to_result: str
        Path to save image
    """
    rescaled_array = (
            255.0 / (numpy_array.max() - numpy_array.min()) * (numpy_array - numpy_array.min())).astype(
        np.uint8)
    im = Image.fromarray(rescaled_array)
    im.save(os.path.join(path_to_result, file_name))


def slice_square_array(array, new_size):
    """
    Function cuts center of given numpy array as square with size new_size
    :param array: numpy array
        Some array to be sliced as square
    :param new_size: int
        Size of square
    :return: array: numpy array
        Squared numpy array of size new_size
    """
    nx, ny = array.shape
    return array[(nx - new_size) // 2: nx - (nx - new_size) // 2, (ny - new_size) // 2: ny - (ny - new_size) // 2]
