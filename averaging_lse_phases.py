from pathlib import Path
from lse_calculation.lse import *
from lse_calculation.auxiliary import *
import time
import matplotlib.pyplot as plt
start = time.time()


def read_holograms(filepath):
    """
    Function reads holograms as 2D numpy arrays and convert it to dict
    with keys as names and values as list of 2D numpy arrays
    :param filepath: str
        A path to directory with holograms as images
    :return: holograms: dict{str:list(2D numpy array)}
        A dictionary of initial holograms, where keys are names of holograms
        or some common part of name in the case of one object hologram set.
        Each element of dictionary contains a list of holograms as 2D int numpy arrays.
    """
    holograms = {}
    print("start reading holos", time.time() - start)
    with os.scandir(filepath) as entries:
        for entry in entries:
            # Read all files one by one
            file_name = entry.name
            full_name = os.path.join(filepath, file_name)
            # This part is only for specific results! Delete it if you have 1 hologram at one object wave
            # Delete extension twice
            name_no_ext = Path(Path(file_name).stem).stem
            if name_no_ext.startswith("Pattern_circle"):
                name_no_ext = "Pattern_circle_20"
            if "cub" in name_no_ext or "circle" in name_no_ext or "square" in name_no_ext:
                # Specific results part ends!
                if name_no_ext not in holograms:
                    holograms[name_no_ext] = []
                holo = Image.open(full_name).convert('L')
                holo = np.asarray(holo, dtype=int)
                holograms[name_no_ext].append(holo)
    print("end reading", time.time() - start)
    return holograms


def average_by_cc(phases_list):
    avr_list = []
    for phase in phases_list:
        average_cc = np.mean(phase[120, 500:530])
        avr_list.append(average_cc)
    average_all_cc = np.mean(avr_list)
    return avr_list, average_all_cc


def eliminate_bad_cc(phases_list, average_all):
    avr_list = []
    for phase in phases_list:
        average_cc = np.mean(phase[120, 500:530])
        epsilon = 0.2
        if abs(average_cc - average_all) < epsilon:
            avr_list.append(phase)
    return avr_list


def divide_ref_obj(rec_waves_dict):
    """
    Function divides a dictionary with names containing circle, square and other keys
    :param rec_waves_dict: dict
        A dictionary of reconstructed complex waves, where keys are names of holograms
        or some common part of name in the case of one object hologram set.
        Each element of dictionary contains a list of reconstructed complex waves as 2D complex numpy arrays.
    :return: circles_dict: dict
        A dictionary of reconstructed complex waves, where keys are names of circle background waves.
        Each element of dictionary contains a list of reconstructed complex waves
        of circle background as 2D complex numpy arrays.
    :return: squares_dict: dict
        A dictionary of reconstructed complex waves, where keys are names of square background waves.
        Each element of dictionary contains a list of reconstructed complex waves
        of square background as 2D complex numpy arrays.
    :return: object_dict: dict
        A dictionary of reconstructed complex waves, where keys are names of object waves reconstructed from holograms.
        Each element of dictionary contains a list of reconstructed complex waves
        of object wave as 2D complex numpy arrays.
    """
    circles_dict, squares_dict, object_dict = {}, {}, {}
    for key in rec_waves_dict:
        if key.__contains__("circle"):
            circles_dict[key] = rec_waves_dict[key]
        elif key.__contains__("square"):
            squares_dict[key] = rec_waves_dict[key]
        else:
            object_dict[key] = rec_waves_dict[key]
    return circles_dict, squares_dict, object_dict


def get_phases_from_dict(waves_dict):
    """
    Function returns dictionary with phases calculated from complex wave dictionary
    :param waves_dict: dict
        A dictionary of reconstructed complex waves, where keys are names of holograms
        or some common part of name in the case of one object hologram set.
        Each element of dictionary contains a list of reconstructed complex waves as 2D complex numpy arrays.
    :return: phases_dict: dict
        A dictionary of phases from complex wave dictionary, where keys are names of holograms
        or some common part of name in the case of one object hologram set.
        Each element of dictionary contains a list of reconstructed phases as 2D complex numpy arrays.
    """
    phases_dict = {}
    for key in waves_dict:
        if key not in phases_dict:
            phases_dict[key] = []
        for wave in waves_dict[key]:
            phases_dict[key].append(np.angle(wave))
    return phases_dict


def create_dirs(filepath_aberrations, folder_name):
    """
    Function creates directory of given folder name to save processed images with a directory to save initial phases.
    Also reads binary mask as numpy array
    :param filepath_aberrations: str
        A path to directory, next to which new directory will be created
    :param filepath_binmask: str
        A path to file - binary mask as image
    :param folder_name: str
        A name of a directory to be created
    :return: path_to_save_subtracted: str
        A path to save result images
    :return: path_to_save_init: str
        A path to save initial phases
    :return: binary_mask: 2D numpy array int
        A binary 2D numpy array as mask
    """
    path_to_save_subtracted = create_result_dir(filepath_aberrations, folder_name)
    path_to_save_init = os.path.join(path_to_save_subtracted, 'initial')
    if not os.path.exists(path_to_save_init):
        os.mkdir(path_to_save_init)
    return path_to_save_subtracted, path_to_save_init


def phases_save(filepath_aberrations, path_init):
    """
    Function reconstructs complex waves from holograms and its reference waves generated by lse method
    and save these phases to given directory with angles between object and reference waves
    calculated by reference waves by lse
    :param filepath_aberrations: str
        A path to holograms to be processed
    :param path_init: str
        A path to save initially reconstructed phases
    :return: rec_phases: dict{str:list(2D float numpy array)}
        A dictionary of reconstructed object phases, where keys are names of holograms
        or some common part of name in the case of one object hologram set.
        Each element of dictionary contains a list of phases as 2D float numpy arrays
    :return: ref_lse_phases: dict{str:2D float numpy array}
        A dictionary of reference phases generated by lse, where keys are names of holograms
        or some common part of name in the case of one object hologram set.
        Each element of dictionary contains a phase of wave as 2D float numpy array

    """
    all_holograms = read_holograms(filepath_aberrations)
    # Reconstruct complex waves and references
    r_0 = 20
    wavelength = 0.532
    pixel_size = 3.75
    window = 10
    print("start reconstructing", time.time() - start)
    rec_waves, ref_lse_waves = reconstruct_complex_wave_from_holograms(
        all_holograms, r_0, pixel_size, wavelength, window)
    print("end reconstructing", time.time() - start)
    # Calculate phases to subsequent processing
    rec_phases = get_phases_from_dict(rec_waves)
    ref_lse_phases = get_phases_from_dict(ref_lse_waves)
    # Save initial phases
    print("start saving initial phases", time.time() - start)
    counter = 1
    for key in rec_phases:
        # If for 1 pattern angles are the same
        ref_lse_phases[key] = ref_lse_phases[key][0]
        for phase in rec_phases[key]:
            vertical_sin, horizontal_sin = find_sin_angles_from_hologram(np.exp(1j * ref_lse_phases[key]), r_0,
                                                                         pixel_size, wavelength)
            angle_vert_degree = vertical_sin * 180 / np.pi
            angle_hor_degree = horizontal_sin * 180 / np.pi
            file_name = key + str(counter) + 'ver' + format(angle_vert_degree, '6f') + 'hor' + format(angle_hor_degree,
                                                                                                      '6f') + '.png'
            save_numpy_array(phase, file_name, path_init)
            counter += 1
    print("end saving initial phases", time.time() - start)
    return rec_phases, ref_lse_phases


def average_all_phases(rec_phases_dict):
    # mean by cc
    print("start averaging", time.time() - start)
    averaged_dict = {}
    result_averaged_dict = {}
    # average_sum = 0
    # for key in rec_phases_dict:
    #     averaged_dict[key] = []
    #     average_list, average_const = average_by_cc(rec_phases_dict[key])
    #     average_sum += average_const
    # average_const_all = average_sum / len(rec_phases_dict)

    for key in rec_phases_dict:
        averaged_dict[key] = []
        average_list, average_const = average_by_cc(rec_phases_dict[key])
        for ind, phase in enumerate(rec_phases_dict[key]):
            ph_const = average_list[ind] - average_const
            u = np.exp(1j * (phase - ph_const))
            averaged_dict[key].append(np.angle(u))
        result_average_list = eliminate_bad_cc(averaged_dict[key], average_const)
        result_averaged_dict[key] = result_average_list
    # mean by all
    result = {}
    for key in result_averaged_dict:
        avr_phase = np.mean(result_averaged_dict[key], axis=0)
        result[key] = avr_phase
    print("end averaging", time.time() - start)
    return result  # , result_averaged_dict


def subtraction(obj_phases_dict, ref_lse_phases_dict, back_phases_dict, back_name, path_to_save_final):
    """
    Function calculates all phase differences between object
    and background complex waves,
    then for each it calculates constant per one array and subtract it,
    then average all phase difference to one 2d numpy array
    :param rec_phases_dict: dict{str:list(2D float numpy array)}
        A dictionary of reconstructed object phases, where keys are names of holograms
        or some common part of name in the case of one object hologram set.
        Each element of dictionary contains a list of phases as 2D float numpy arrays
    :param ref_lse_phases_dict: dict{str:2D float numpy array}
        A dictionary of reference phases generated by lse method for tilt elimination,
        where keys are names of holograms or some common part of name in the case of one object hologram set.
        Each element of dictionary contains a list of reference phases as 2D float numpy arrays
    :param back_phases_dict: dict{str:list(2D float numpy array)}
        A dictionary of reconstructed background phases with 1 key as circle or square DMD-pattern.
        Each element of dictionary contains a list of background phases as 2D float numpy arrays
    :param back_name: str
        A name of background - circle or square
    :param binary_mask: numpy array int
        A binary 2D numpy array to eliminate all phase noise
    :return: result: dict{str:numpy.array}
        A dictionary of calculated averaged phase differences with subtracted phase constant,
        where key is name of array to be saved and value is 2D numpy array
    """
    averaged_phase_diffs = {}
    print("start subtracting", time.time() - start)
    # Subtracting phases
    result_dict = {}
    for key in obj_phases_dict:
        for obj_phase in obj_phases_dict[key]:
            key_back = list(back_phases_dict.keys())[0]
            key_back_save = key + back_name + ".png"
            if key_back_save not in result_dict:
                result_dict[key_back_save] = []
            for back_phase in back_phases_dict[key_back]:
                # Calculate all phase_diffs for circle background
                tilt = np.divide(np.exp(1j * ref_lse_phases_dict[key_back]), np.exp(1j * ref_lse_phases_dict[key]))
                phase_subtracted = np.angle(np.divide(np.divide(np.exp(1j * obj_phase), np.exp(1j * back_phase)), tilt))

                result_dict[key_back_save].append(phase_subtracted)
    aver_dict = average_all_phases(result_dict)
    print("end subtracting", time.time() - start)
    for key_back_save, phase_diff in aver_dict.items():
        save_numpy_array(phase_diff, key_back_save, path_to_save_final)
    return averaged_phase_diffs


filepath_hol = r"C:\Users\alexandra\YandexDisk\ITMO\2022\papers\appsci_aberrations\angles2"
folder_result_name = "new_average_test"

# filepath_hol = r"path_to_holograms_here"
# folder_result_name = "folder_name_here"
path_to_save, path_to_init = create_dirs(filepath_hol, folder_result_name)

rec_phases_all, ref_lse_phases_all = phases_save(filepath_hol, path_to_init)

# averaged_phases = average_all_phases(rec_phases_all)
circles_phase, squares_phase, object_phases = divide_ref_obj(rec_phases_all)
subtraction(object_phases, ref_lse_phases_all, circles_phase, "_circle", path_to_save)
subtraction(object_phases, ref_lse_phases_all, squares_phase, "_square", path_to_save)


end = time.time()
print(end - start)
