# import all modules

from math import ceil
from os.path import abspath
from tkinter import (END, RAISED, Button, Canvas, Entry, Frame, Label,
                     OptionMenu, StringVar, Text, Tk, ttk, Toplevel)
from tkinter.filedialog import askopenfilename, asksaveasfile

import numpy as np
from PIL import Image, ImageTk, UnidentifiedImageError
from scipy import signal
from scipy.io import savemat
from webbrowser import open_new_tab


def callback(url):
    open_new_tab(url)


class HoloApp:
    def __init__(self, master):
        self.but_help = Button(master, width=1, height=1, text="?", font=("Verdana", 15))
        self.but_help.grid(row=0, column=5, ipadx=4, sticky="ne")
        self.frame_path = Frame(master, bg="mint cream")
        self.frame_path.grid(row=1, column=0, rowspan=2, columnspan=3, sticky="nsew", padx=10)
        self.warn_text = Label(master, text="Fill in all the fields",
                               font=("Verdana", 15, "bold"), bg="mint cream")
        self.warn_text.grid(row=1, column=3, rowspan=1, columnspan=2, sticky="nsew")

        self.frame_method = Frame(bg="mint cream")
        self.frame_method.grid(row=4, column=0, rowspan=6, columnspan=3, sticky="nsew", padx=10)
        self.but_rec = Button(master, height=1, text="Reconstruct", font=("Verdana", 15))
        self.but_rec.grid(row=3, column=0, columnspan=3, ipadx=2, padx=5)
        self.frame_param = Frame(self.frame_method, bg="mint cream")
        self.frame_param.grid(row=3, column=0, columnspan=3)
        # self.method_bar = MethodModule(self.frame_path, self.frame_method, self.frame_param, self.warn_text)
        self.tab_control = ttk.Notebook(root)
        tab_name_list = ["Hologram", "Amplitude", "Phase"]
        self.tabs = [None] * len(tab_name_list)

        for ind, tab_name in enumerate(tab_name_list):
            self.tabs[ind] = ttk.Frame(self.tab_control)
            self.tab_control.add(self.tabs[ind], text=tab_name)
        self.tab_control.grid(row=2, column=3, rowspan=9, columnspan=2, sticky="nsew")

        self.reconstruction_bar = ReconstructModule(self.frame_path, self.frame_method, self.frame_param,
                                                    self.warn_text, self.but_rec, self.tabs, self.tab_control)
        self.but_help.configure(command=lambda: self.create_help_window())

    def create_help_window(self):
        self.help_window = Toplevel()
        self.link = Label(master=self.help_window, text='Лабораторный практикум "Цифровая голография"',
                          font=('Verdana', 15), fg="blue", cursor="hand2")
        self.link.grid(row=0, column=0)
        self.link.bind("<Button-1>", lambda e: callback("https://books.ifmo.ru/file/pdf/2749.pdf"))
        self.lbl_help = Label(master=self.help_window, font=("Verdana", 10))
        self.lbl_help["text"] = """Данная программа предназначена для восстановления амплитудно-фазовых характеристик из внеосевых цифровых голограмм. 
        На выбор предлагаются два метода - метод Фурье-фильтрации и метод локальных наименьших квадратов. 
        Подробное описание каждого метода представлено в методическом пособии по ссылке выше (кликабельно!).
        Алгоритм работы с программой:
        1) Выбрать внеосевую голограмму в растровом формате с помощью кнопки "Load" или ввести путь к голограмме в поле для ввода.
        2) Выбрать метод восстановления из выпадающего списка: Fourier Filtering или Least Squares Estimation.
        3) Ввести параметры для восстановления:
            - Фурье-фильтрация:
                a) r_0 - радиус для вырезания нулевого порядка дифракции в пикселях.
                Необходим для дальнейшего поиска расположения первого порядка дифракции.
                b) r_1 - радиус для фильтрации первого порядка дифракции в пикселях.
                Радиус окна фильтрации первого порядка дифракции
            - Локальных наименьших квадратов:
                a) r_0 - радиус для вырезания нулевого порядка дифракции в пикселях.
                Необходим для дальнейшего поиска расположения первого порядка дифракции,
                а также вычисления проекций к OX и OY угла наклона между опорной и объектной волнами.
                b) \u0394 - Размер пикселя камеры в микрометрах
                c) \u03BB - Длина волны в микрометрах
                d) w - размер оконной функции в пикселях.
                Необходимо подбирать приблизительно равным периоду голограммы в пикселях 
        4) Нажать кнопку "Reconstruct"
        В табах будут отображены исходная голограмма, амплитуда и фаза полученного комплексного поля
        Для сохранения полученного распределения нажмите кнопку "Save as". В открывшемся окне выберите имя и тип сохранения распределения. 
        Доступны растровые графические форматы, а также .csv, .npy и .mat форматы.
        В случае необходимости дальнейшей обработки полученных распределений рекомендуется .npy, .mat или .csv формат.
        5) Кнопка "Show spectrum" откроет новое окно, в котором будут показаны спектр голограммы, спектр с вырезанным нулевым порядком, 
        а также в случае метода ФФ смещенный в центр первый порядок, а в случае метода ЛНК - фаза синтезированной плоской опорной волны.
        Данные распределения также могут быть сохранены нажатием кнопки "Save as"
        NB! При сохранении в форматах .mat, npy, .csv будут сохранены комплексные массивы!
        Для ФФ будет показано рассчитанное в пикселях расстояние от 0 до 1 порядка дифракции.
        Для ЛНК будут показаны проекции угла между опорной и объектной волнами к оси OX и оси OY в градусах и расстояние от 0 до 1 порядка.

        Если у вас возникнет желание поковыряться в исходном коде, можно обратиться к первоисточнику данной программы (на Python) по ссылке ниже.

        Успехов! 
        """
        self.lbl_help.grid(row=1, column=0, sticky="w")
        self.link2 = Label(master=self.help_window, text='Исходник на Python',
                           font=('Verdana', 15), fg="blue", cursor="hand2")

        self.link2.grid(row=2, column=0)
        self.link2.bind("<Button-1>", lambda e: callback(
            "https://gitlab.com/Alienna/hologram_reconstruction/-/tree/master/off-axis_reconstruction_interface"))


class PathModule(HoloApp):
    def __init__(self, frame_path, warn_text):
        self.lbl_path = Text(master=frame_path)
        self.lbl_path.configure(font=("Verdana", 9), height=4, width=30)
        self.lbl_path.insert(1.0, "Press the button 'Load' to choose a hologram")
        self.warning = warn_text
        self.but_path = Button(
            master=frame_path,
            text="Load",
            command=lambda: self.open_file(),
            font=("Verdana", 12, "bold"),
            padx=35,
        )
        self.but_path.grid(row=0, column=1, columnspan=3, pady=5)
        self.lbl_path.grid(row=1, column=1, columnspan=3)
        HoloApp.tkvarq1 = StringVar(frame_path)
        HoloApp.tkvarq1.set(self.lbl_path.get(1.0, END))

    # Open file func
    def extract_pic(self, filepath):
        try:
            holo = Image.open(filepath).convert('L')
            self.lbl_path.delete(1.0, END)
            self.lbl_path.insert(1.0, filepath)
            self.warning["text"] = "Image loaded"
            return np.array(holo, dtype=int)
        except AttributeError:
            self.warning["text"] = "No new image selected"
        except FileNotFoundError:
            self.warning["text"] = "There is no such file! Please check file path"
        except UnidentifiedImageError:
            self.warning["text"] = "Not an image! Please check file path"

    def open_file(self):
        filepath = askopenfilename(filetypes=[("All Files", "*.*")])
        self.extract_pic(filepath)


class MethodModule(PathModule):
    def __init__(self, frame_path, frame_method, frame_param, warn_text):
        super().__init__(frame_path, warn_text)
        self.frame_method = frame_method
        self.frame_param = frame_param
        methods = ["Fourier Filtering", "Least Squares Estimation"]
        HoloApp.tkvarq = StringVar(self.frame_method)
        HoloApp.tkvarq.set(methods[1])
        self.list_method = OptionMenu(self.frame_method, HoloApp.tkvarq, *methods)
        self.list_method.config(width=20)

        self.lbl_method = Label(
            self.frame_method,
            text="Reconstruction method",
            font=("Verdana", 12),
            pady=6,
            bg="mint cream"
        )
        HoloApp.tkvarq.trace("w", self.show_param)
        self.list_method.config(font=("Verdana", 12))
        self.lbl_method.grid(row=1, column=0, columnspan=3, sticky="nsew")
        self.list_method.grid(row=2, column=0, columnspan=3, sticky="nsew")
        self.wth_path = str(self.lbl_path.get(1.0, END))[:-1]

    def hide_widget(self, widget):
        for param in widget.grid_slaves():
            param.grid_remove()

    def func_param(self, widget, L, L_unit, L_bounds, L_default, L_stepsize):
        widget.grid(row=3)
        for idx, txt in enumerate(L):
            rec_params = InputParamModule(
                self.frame_param,
                idx,
                txt,
                L_unit[idx],
                L_default[idx],
                L_bounds[idx],
                L_stepsize[idx],
            )

    def show_param(self, *args):
        if self.wth_path == "Press the button to choose a hologram":
            ff_bounds = [(0, 512), (0, 512)]
            lse_bounds = [(0, 512), (0, 512), (0, 512), (0, 512)]
        else:
            output = self.extract_pic(self.lbl_path.get(1.0, END)[:-1])
            ff_bounds = [output.shape, output.shape]
            lse_bounds = [
                (0, output.shape),
                (0, 512),
                (0, 512),
                (0, output.shape),
            ]

        ff_param = ["r0 ", "r1 "]
        ff_units = ["px", "px"]
        ff_stepsize = [1, 1]
        lse_param = ["r0 ", "\u0394 ", "\u03BB ", "w"]
        ff_default = [
            (ff_bounds[0][1] - ff_bounds[0][0]) // 2,
            (ff_bounds[1][1] - ff_bounds[1][0]) // 2,
        ]
        lse_units = ["px", "\u03BCm", "\u03BCm", "px"]
        lse_default = [10, 4.0, 0.532, 10]
        lse_stepsize = [1, 0.1, 0.1, 1]

        if HoloApp.tkvarq.get() == "Fourier Filtering":
            self.hide_widget(self.frame_param)
            self.func_param(
                self.frame_param, ff_param, ff_units, ff_bounds, ff_default, ff_stepsize
            )

        elif HoloApp.tkvarq.get() == "Least Squares Estimation":
            self.hide_widget(self.frame_param)
            self.func_param(
                self.frame_param,
                lse_param,
                lse_units,
                lse_bounds,
                lse_default,
                lse_stepsize,
            )


class InputParamModule(MethodModule):
    def __init__(
            self, frame_param, idx, lbl_param, unit_param, default, bounds, stepsize
    ):
        self.idx = idx
        self.unit_param = unit_param
        self.default = default
        self.bounds = bounds
        self.stepsize = stepsize
        self.frame_1_param = Frame(frame_param, bg="mint cream")
        self.frame_1_param.grid(row=self.idx, sticky="w")
        self.lbl_param = Label(
            self.frame_1_param, text=lbl_param, font=("Verdana", 9), bg="mint cream"
        )
        self.ent_param = Entry(self.frame_1_param, width=7, font=("Verdana", 9))
        self.but_plus = Button(
            self.frame_1_param,
            text="+",
            height=1,
            width=1,
            command=lambda: self.plus_step(),
            font=("Verdana", 4),
        )
        self.but_minus = Button(
            self.frame_1_param,
            text="-",
            height=1,
            width=1,
            command=lambda: self.minus_step(),
            font=("Verdana", 4),
        )
        self.lbl_unit = Label(
            self.frame_1_param,
            text=self.unit_param,
            font=("Verdana", 9),
            bg="mint cream",
        )

        self.ent_param.insert(0, self.default)
        self.lbl_param.grid(row=0, column=0, rowspan=2, sticky="w", pady=3)
        self.ent_param.grid(row=0, column=1, rowspan=2, sticky="e", pady=3)
        self.but_plus.grid(row=0, column=2, sticky="se")
        self.but_minus.grid(row=1, column=2, sticky="ne")
        self.lbl_unit.grid(row=0, column=3, rowspan=2, sticky="w", pady=3)

    def set_num(self, number):
        self.ent_param.delete(0, END)
        self.ent_param.insert(0, number)

    def plus_step(self):
        res = float(self.ent_param.get()) + self.stepsize
        if float(self.bounds[1]) > res > float(self.bounds[0]):
            self.set_num(res)
        else:
            pass

    def minus_step(self):
        res = float(self.ent_param.get()) - self.stepsize
        if float(self.bounds[1]) > res > float(self.bounds[0]):
            self.set_num(res)


def matlab_style_gauss2d(shape=(3, 3), sigma=0.5):
    """
    2D gaussian mask - should give the same result as MATLAB's
    fspecial('gaussian',[shape],[sigma])
    ----------
    shape : tuple, optional
        size of the filter. The default is (3,3).
    sigma : float, optional
        standart deviation. The default is 0.5.

    Returns
    -------
    h : numpy array
    2D gaussian filter
    """
    m, n = [(ss - 1.0) / 2.0 for ss in shape]
    y, x = np.ogrid[-m: m + 1, -n: n + 1]
    h = np.exp(-(x * x + y * y) / (2.0 * sigma * sigma))
    h[h < np.finfo(h.dtype).eps * h.max()] = 0
    sumh = h.sum()
    if sumh != 0:
        h /= sumh
    return h


def cutout_diffraction_order(some_spectra, **kwargs):
    """
    Function cuts the zeroth diffraction order in case of r_0 in kwargs
    or cuts everything around the first diffraction order of r_1 in kwargs.
    If center in kwargs, cuts circle area of radius r_0
    Parameters
    ----------
    :param some_spectra : numpy array complex
        angular spectrum with diffraction orders.
    :param **kwargs:
        See below

    :Keyword Arguments:
        r_0 : int, optional
        Radius of cutting the 0 diffraction order.
        r_1 : int, optional
            Radius of the 1 diffraction order.
            Perform cutting out everything except 1 order.

    Returns
    -------
    :return: new_spectrum : complex
        Spectra with the 1 diffraction order of radius=r_1 or spectra without
        the 0 diffraction order of a radius r_0.
    """
    ny, nx = some_spectra.shape
    center_y, center_x = ny // 2, nx // 2
    ii = np.arange(ny)[:, np.newaxis]
    jj = np.arange(nx)
    for key, value in kwargs.items():
        if key == "r_0":
            r_current = value
            res = np.where(np.sqrt((ii - center_y) ** 2 + (jj - center_x) ** 2) < r_current, 0, some_spectra)
            return res
        elif key == "r_1":
            r_current = value  # ny / value / 2
            res = np.where(np.sqrt((ii - center_y) ** 2 + (jj - center_x) ** 2) < r_current, some_spectra, 0)
            return res


def fourier_filtering(hologram, **kwargs):
    """
    Function process hologram data with some filtering parameters
    and saves csv file with phase distribution and information about anomaly presence
    Parameters
    ----------
    :param hologram: numpy array (int)
        2D distribution of initial hologram.
    :param: **kwargs
        See below

        Keyword arguments
            r_1 : int
                Radius of the 1st diffraction order intending.
            r_0 : int
                Radius of the 0th diffraction order cutting out.
    Returns
    -------
    reconstructed_field : numpy array (complex)
        Complex reconstructed field.
    """
    ny, nx = hologram.shape
    spectrum_init = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(hologram)))
    spectrum = abs(spectrum_init)
    new_spectrum = cutout_diffraction_order(spectrum, r_0=kwargs["r_0"])
    max_intensity_index = np.unravel_index(new_spectrum.argmax(), new_spectrum.shape)
    shy, shx = max_intensity_index[0] - ny // 2, max_intensity_index[1] - nx // 2
    centered_spectrum = np.zeros((ny, nx), dtype=complex)
    centered_spectrum[max(shy, 0):ny + min(shy, 0), max(shx, 0):nx + min(shx, 0)] = spectrum_init[
                                                                                    -min(shy, 0):ny - max(shy, 0),
                                                                                    -min(shx, 0):nx - max(shx, 0),
                                                                                    ]
    first_diforder = cutout_diffraction_order(centered_spectrum, r_1=kwargs["r_1"])
    reconstructed_field = np.fft.fftshift(
        np.fft.ifft2(np.fft.ifftshift(first_diforder))
    )

    return reconstructed_field, spectrum_init, new_spectrum, first_diforder, max_intensity_index


def find_sin_angles_from_hologram(hologram, r_0, pixel_size, wavelength):
    """
    Function returns sinuses of vertical and horizontal components
    of angle between object and reference wave in hologram
    :param hologram: numpy array int
        A hologram as 2D numpy int array
    :param r_0: int
        Radius of cutting the 0 diffraction order
    :param pixel_size: float
        Size of pixel in micrometers
    :param wavelength: float
        Wavelength in micrometers
    :return: vertical_sin: float
        Sinus of vertical component of angle between reference and object wave
    :return: horizontal_sin: float
        Sinus of horizontal component of angle between reference and object wave
    """
    ny, nx = hologram.shape
    spectrum_init = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(hologram)))
    spectrum = abs(spectrum_init)
    new_spectrum = cutout_diffraction_order(spectrum, r_0=r_0)
    max_intensity_index = np.unravel_index(new_spectrum.argmax(), new_spectrum.shape)
    shift_y = max_intensity_index[0] - ny // 2
    shift_x = max_intensity_index[1] - nx // 2
    vertical_sin = shift_y * wavelength / ny / pixel_size
    horizontal_sin = shift_x * wavelength / nx / pixel_size
    return vertical_sin, horizontal_sin, max_intensity_index


def least_squares(
        hologram,
        r_0,
        pixel_size,
        wavelength,
        window_size,
):
    """
    Function calculates reconstructed complex wave by least-squares estimation method
    (Michael Liebling, Thierry Blu, and Michael Unser, "Complex-wave retrieval from a single off-axis hologram,"
    J. Opt. Soc. Am. A 21, 367-377 (2004))
    with auto determination of angle between object and reference wave
    by calculation of first diffraction order coordinates in initial hologram spectrum

    Parameters
    ----------
    :param hologram : numpy array int
        Intensity hologram to be reconstructed
    :param r_0 : int
        Radius of cutting the 0 diffraction order in pixels
    :param pixel_size: float
        Size of pixel in micrometers
    :param wavelength: float
        Wavelength in micrometers
    :param window_size: int
        Weighting window for a B-spline
    Returns
    ----------
    :return: reconstructed_wave : numpy array complex
        Reconstructed wave
    :return: ref_wave : numpy array complex
        Generated reference wave with same angle between object and reference wave as hologram
    """
    if window_size < 3:
        window_size = 4
    if window_size % 2 != 0:
        window_size += 1
    vertical_sin, horizontal_sin, coord_1st_ord = find_sin_angles_from_hologram(hologram, r_0, pixel_size, wavelength)
    ny, nx = hologram.shape
    sinuses_lse = vertical_sin, horizontal_sin
    spectrum_init = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(hologram)))
    spectrum_cut = cutout_diffraction_order(spectrum_init, r_0=r_0)
    # print(np.arcsin(vertical_sin)*180/np.pi)
    # print(np.arcsin(horizontal_sin)*180/np.pi)
    ii = np.arange(ny)[:, np.newaxis]
    jj = np.arange(nx)
    ref_wave = np.exp(
        ((ii + 1) * vertical_sin + (jj + 1) * horizontal_sin)
        / wavelength * 1j * 2 * np.pi * pixel_size)

    weight = matlab_style_gauss2d((window_size, window_size), window_size / 6)
    kk = signal.fftconvolve(hologram, weight)
    ll = signal.fftconvolve(hologram * ref_wave, weight)
    m = np.conj(ll)
    a = signal.fftconvolve(weight, ref_wave)
    b = signal.fftconvolve(weight, ref_wave * ref_wave)
    reconstructed_field = np.round(
        (
                ll * np.conj(b)
                + m * (abs(a) * abs(a))
                + kk * np.conj(a)
                - kk * a * np.conj(b)
                - m
                - ll * np.conj(a) * np.conj(a)
        )
        / (
                abs(b) * abs(b)
                + 2 * abs(a) * abs(a)
                - np.conj(b) * a * a
                - 1
                - np.conj(a) * np.conj(a) * b
        ),
        3,
    )
    nx2 = len(reconstructed_field[0])
    ny2 = len(reconstructed_field)
    rounded = ceil(window_size / 2) - 1
    reconstructed_wave = reconstructed_field[
                         rounded: (ny2 - rounded - 1), rounded: (nx2 - rounded - 1)
                         ]
    return reconstructed_wave, ref_wave, spectrum_init, spectrum_cut, coord_1st_ord, sinuses_lse


def resize_image(im_size, canv_size):
    y, x = canv_size
    ny, nx = im_size
    nx_r = nx
    ny_r = ny
    y1, x1 = y, x
    while nx_r < x:
        nx_r = nx_r * x1 // y1
        ny_r = ny_r * x1 // y1
        y1 = nx_r
        x1 = x
    while ny_r < y:
        nx_r = nx_r * x1 // y1
        ny_r = ny_r * x1 // y1
        y1 = ny_r
        x1 = y
    while nx_r > x:
        nx_r = nx_r * y1 // x1
        ny_r = ny_r * y1 // x1
        y1 = x
        x1 = nx_r
    while ny_r > y:
        y1 = y
        x1 = ny_r
        nx_r = nx_r * y1 // x1
        ny_r = ny_r * y1 // x1
    return nx_r, ny_r


class ReconstructModule(MethodModule):
    def __init__(self, frame_path, frame_method, frame_param, warn_text, but_rec, tabs, tab_control):
        super().__init__(frame_path, frame_method, frame_param, warn_text)
        self.show_dict = None
        self.but_rec = but_rec
        self.tab_control = tab_control
        self.tabs = tabs
        self.result_images = {}
        self.but_save = Button(text="Save as")
        self.but_spec = Button(text="Show spectrum", font=("Verdana", 12))
        self.but_new_window = Button(text="Show auxiliary images")
        self.frame_ph_const = Frame(bg="mint cream")
        self.lbl_ph_const = Label(self.frame_ph_const, text="Phase constant \u03C0*", bg="mint cream",
                                  font=("Verdana", 10))
        self.ph_const = Entry(self.frame_ph_const)
        self.ph_const.insert(0, "0")
        self.but_ph_const = Button(self.frame_ph_const, text="Add phase constant", font=("Verdana", 10))
        self.frame_ph_const.grid(row=9, column=0, columnspan=3, rowspan=2)
        self.lbl_ph_const.grid(row=0, column=0, sticky="e")
        self.ph_const.grid(row=0, column=1, sticky="w")
        self.but_ph_const.grid(row=1, column=0, columnspan=2)

        self.but_rec.configure(
            text="Reconstruct!",
            font=("Verdana", 12, "bold"),
            command=lambda: self.reconstruction(),
        )
        # self.image_canvas = [None] * 3
        self.image_canvas = {}
        self.tab_name_list = ["Hologram", "Amplitude", "Phase"]
        for ind, tab in enumerate(self.tabs):
            self.image_canvas[self.tab_name_list[ind]] = Canvas(
                tab,
                width=800,
                height=500,
                bd=0,
                highlightthickness=0,
                bg="mint cream",
                relief=RAISED,
            )
        for key in self.image_canvas:
            self.image_canvas[key].grid(row=0, column=0)

    def reconstruction(self):
        holo_data = self.extract_pic(self.lbl_path.get(1.0, END)[:-1])
        var_param = []
        try:
            for param in self.frame_param.grid_slaves():
                if param.winfo_class() == "Frame":
                    for par in param.grid_slaves():
                        if par.winfo_class() == "Entry":
                            var_param.append(float(par.get()))
        except IndexError:
            self.warning["text"] = "Choose reconstruction method"
        if HoloApp.tkvarq.get() == "Fourier Filtering":
            try:
                reconstructed, spec1, spec2, spec3, coord_1st_ord = fourier_filtering(
                    holo_data, r_1=int(var_param[0]), r_0=int(var_param[1])
                )
                self.warning["text"] = "Reconstructed by FF"
                self.show_dict = {"Hologram": holo_data, "Amplitude": np.abs(reconstructed),
                                  "Phase": np.angle(reconstructed)}
                for key in self.image_canvas:
                    self.show_image_in_tab(self.show_dict[key], self.image_canvas[key], key)
                self.but_save.configure(command=lambda: self.save_me(self.tab_control, self.show_dict), font=("Verdana",
                                                                                                              12))
                self.but_save.grid(row=11, column=4, padx=5)
                self.but_spec.configure(
                    command=lambda: self.create_new_window(coord_1st_ord, spec1=spec1, spec2=spec2, spec3=spec3))
                self.but_spec.grid(row=11, column=3, padx=5)
                self.but_ph_const.configure(command=lambda: self.add_ph_const(self.show_dict["Phase"]))
            except ValueError:
                self.warning["text"] = "Error! Please check your input parameters"
        elif HoloApp.tkvarq.get() == "Least Squares Estimation":
            try:
                reconstructed, refwave, spec1, spec2, coord_1st_ord, self.sinuses = least_squares(
                    holo_data,
                    int(var_param[3]),
                    float(var_param[2]),
                    float(var_param[1]),
                    int(var_param[0]),
                )
                self.warning["text"] = "Reconstructed by LSE"
                self.show_dict = {"Hologram": holo_data, "Amplitude": np.abs(reconstructed),
                                  "Phase": np.angle(reconstructed)}

                self.but_save.configure(command=lambda: self.save_me(self.tab_control, self.show_dict),
                                        font=("Verdana", 12))
                self.but_save.grid(row=11, column=4, padx=5)
                self.but_spec.configure(
                    command=lambda: self.create_new_window(coord_1st_ord, spec1=spec1, spec2=spec2, refwave=refwave))
                self.but_spec.grid(row=11, column=3, padx=5)
                for key in self.image_canvas:
                    self.show_image_in_tab(self.show_dict[key], self.image_canvas[key], key)
                self.but_ph_const.configure(command=lambda: self.add_ph_const(self.show_dict["Phase"]))
            except UnidentifiedImageError:
                self.warning["text"] = "Error! Please check your input parameters"

    def show_image_in_tab(self, array_to_show, current_canvas, name):
        if "spec" in name or "order" in name:
            array_to_show = np.abs(array_to_show)
            result = np.where(array_to_show > 1.0e-10, array_to_show, 1)
            array_to_show = np.log(result, out=result, where=result > 0)
        elif "Ref" in name:
            array_to_show = np.angle(array_to_show)
        if array_to_show.dtype == float:
            array_to_show = (
                    (array_to_show - array_to_show.min())
                    * (1 / (array_to_show.max() - array_to_show.min()) * 255)
            ).astype("uint8")
        img = Image.fromarray(array_to_show)
        new_im_shape = resize_image(array_to_show.shape, (500, 800))
        current_canvas.image = ImageTk.PhotoImage(img.resize(new_im_shape))
        self.result_images[name] = current_canvas.image
        hohoho = current_canvas.image
        current_canvas.create_image(0, 0, image=hohoho, anchor="nw")

    def add_ph_const(self, phase):
        ph_const_str = self.ph_const.get()
        if "/" in ph_const_str:
            list_a = ph_const_str.split("/")
            ph_const_num = float(list_a[0]) / float(list_a[1])
        else:
            ph_const_num = float(ph_const_str)
        new_phase = np.angle(np.exp(1j * (phase + ph_const_num * np.pi)))
        self.show_dict["Phase"] = new_phase
        self.show_image_in_tab(new_phase, self.image_canvas["Phase"], "Phase")
        self.warning["text"] = "Added phase constant"

    def save_me(self, used_tab_control, dict_to_save):
        current_tab_name = used_tab_control.tab(used_tab_control.select(), "text")
        data = dict_to_save[current_tab_name]
        file = asksaveasfile(title="Save me", defaultextension=".png",
                             filetypes=(("PNG files", "*.png"),
                                        ("BMP files", "*.bmp"),
                                        ("TIFF files", "*.tiff"),
                                        ("JPG files", "*.jpg"),
                                        ("MAT files", "*.mat"),
                                        ("NPY files", "*.npy"),
                                        ("CSV files", "*.csv"),
                                        ("All files", "*.*")))
        try:
            abs_path = abspath(file.name)
            if ".npy" in abs_path:
                np.save(abs_path, data)
                self.warning["text"] = current_tab_name + " saved as .npy"
            elif ".mat" in abs_path:
                savemat(abs_path, {current_tab_name: data})
                self.warning["text"] = current_tab_name + " saved as .mat"
            elif ".csv" in abs_path:
                np.savetxt(abs_path, data, delimiter=',')
                self.warning["text"] = current_tab_name + " saved as .csv"
            elif ".png" in abs_path or ".jpg" in abs_path or ".jpeg" in abs_path or ".tif" in abs_path or \
                    ".bmp" in abs_path or ".tiff" in abs_path:
                if "spec" in current_tab_name or "order" in current_tab_name:
                    data = np.abs(data)
                    result = np.where(data > 1.0e-10, data, -10)
                    data = np.log(result, out=result, where=result > 0)
                elif "Ref" in current_tab_name:
                    data = np.angle(data)
                rescaled = (255.0 / (data.max() - data.min()) * (data - data.min())).astype(np.uint8)
                im = Image.fromarray(rescaled)
                im.save(abs_path)
                self.warning["text"] = current_tab_name + " saved as image"
            elif ".txt" in abs_path:
                np.savetxt(abs_path, data)
                self.warning["text"] = current_tab_name + " saved as txt"
            else:
                self.warning["text"] = "Error! Please choose one of suggested options to save " + current_tab_name
            file.close()
        except AttributeError:
            self.warning["text"] = current_tab_name + " was not saved"

    def create_new_window(self, ind_1st_ord, **kwargs):
        self.add_window = Toplevel()
        self.add_images = []
        self.calc_labels = Label(master=self.add_window, font=("Verdana", 9))
        self.but_save_spec = Button(master=self.add_window, text="Save as", font=("Verdana", 12))
        ny, nx = kwargs["spec1"].shape
        distance01 = np.sqrt((ny // 2 - ind_1st_ord[0]) ** 2 + (nx // 2 - ind_1st_ord[1]) ** 2)
        if HoloApp.tkvarq.get() == "Fourier Filtering":
            tab_name_list_spec = ["Initial_spectrum", "Cutout_0th_order", "Intend_1st_order"]
            tab_name_spec_vars = ["spec1", "spec2", "spec3"]
            self.calc_labels["text"] = f"distance 0 to 1 order={round(distance01)}px"
        elif HoloApp.tkvarq.get() == "Least Squares Estimation":
            tab_name_list_spec = ["Initial_spectrum", "Cutout_0th_order", "Reference_wave_phase"]
            tab_name_spec_vars = ["spec1", "spec2", "refwave"]
            self.calc_labels["text"] = f"angle y={round(np.arcsin(self.sinuses[0]) * 180 / np.pi, 4)}\u00B0\n" \
                                       f"angle x={round(np.arcsin(self.sinuses[1]) * 180 / np.pi, 4)}\u00B0\n" \
                                       f"distance 0 to 1 order={round(distance01)}px"
        self.new_dict_spec = {}
        for key in kwargs:
            ind = tab_name_spec_vars.index(key)
            var_key = tab_name_list_spec[ind]
            self.new_dict_spec[var_key] = kwargs[key]
        self.tab_control_spec = ttk.Notebook(master=self.add_window)
        # self.tab_control_spec.enable_traversal()
        self.tabs_spec = [None] * len(tab_name_list_spec)
        for ind, tab_name in enumerate(tab_name_list_spec):
            self.tabs_spec[ind] = ttk.Frame(self.tab_control_spec)
            self.tab_control_spec.add(self.tabs_spec[ind], text=tab_name)
        self.tab_control_spec.grid(row=0, column=0, columnspan=2)
        self.image_canvas_spec = {}
        self.but_save_spec.configure(command=lambda: self.save_me(self.tab_control_spec, self.new_dict_spec))
        self.but_save_spec.grid(row=1, column=1)
        for ind, tab in enumerate(self.tabs_spec):
            self.image_canvas_spec[tab_name_list_spec[ind]] = Canvas(
                tab,
                width=800,
                height=500,
                bd=0,
                highlightthickness=0,
                bg="mint cream",
                relief=RAISED,
            )
        for key in self.image_canvas_spec:
            self.image_canvas_spec[key].grid(row=0, column=0)
        self.calc_labels.grid(row=1, column=0, sticky="w")
        for key, value in self.image_canvas_spec.items():
            self.show_image_in_tab(self.new_dict_spec[key], value, key)


root = Tk()
root.title('Laboratory work "Digital off-axis hologram reconstruction"')
screen_width = root.winfo_screenwidth()
screen_height = root.winfo_screenheight()
root.minsize(1100, 700)

root.columnconfigure(3, weight=8)
root.configure(bg="mint cream")
app = HoloApp(root)
root.mainloop()
