import numpy as np
from propagation import propagator
import time
import matplotlib.pyplot as plt
from PIL import Image


def make_complex_field_from_image(intensity_path, phase_path):
    intensity_image = Image.open(intensity_path).convert('L')
    intensity = np.asarray(intensity_image, dtype=int)
    intensity = np.minimum(1, intensity / 255 * 1.3)
    phase_image = Image.open(phase_path).convert('L')
    # phase = np.asarray(phase_image, dtype=int) / 255 * 2 * np.pi
    # u_target = np.sqrt(intensity) * np.exp(1j * (phase + np.pi))
    phase = np.asarray(phase_image, dtype=int)
    u_target = np.sqrt(intensity) * np.exp(1j * (1.9 * np.pi * phase / np.max(phase) - 0.95 * np.pi))
    return u_target


def complex_to_dmd_pixels(u_target, ky, kx, wavelength, pix, z):
    ny, nx = u_target.shape
    u_propagated = propagator.propagate(u_target, wavelength, pix, -z)
    amplitude = np.abs(u_propagated)
    phase = np.angle(u_propagated)
    ii = np.arange(ny)[:, np.newaxis]
    jj = np.arange(nx)
    ref_wave = np.exp(-2 * 1j * np.pi * (ii * ky + jj * kx + 0.5))
    dmd_pixels = np.where(
        np.arcsin(amplitude) / 2 / np.pi >= np.abs(np.mod(
            phase / 2 / np.pi + (ii * ky + jj * kx), 1) - 0.5), 1, 0)
    return dmd_pixels * ref_wave


def fourier_mask(some_spectra, **kwargs):
    """
    Parameters
    ----------
    some_spectra : numpy array complex
        angular spectrum with diffraction orders.
    aperture : int, optional
        Radius of cutting the 0 diffraction order.
    Returns
    -------
    new_spectrum : complex
        Spectra without the 0 diffraction order of a radius r_0.
    """
    ny, nx = some_spectra.shape
    center_x = nx // 2
    center_y = ny // 2
    ii = np.arange(ny)[:, np.newaxis]
    jj = np.arange(nx)
    for key, value in kwargs.items():
        if key == "resolution":
            radius = ny / value / 2
        elif key == "aperture":
            radius = value / 2
        else:
            raise KeyError("please use key arguments 'resolution' or 'aperture'")
    res = np.where(radius > np.sqrt((ii - center_y) ** 2 + (ny / nx * (jj - center_x)) ** 2), some_spectra, 0)
    return res


def system_4f(dmd_pattern, wavelength, pix, z, **kwargs):
    first_lens = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(dmd_pattern)))
    filtered = fourier_mask(first_lens, **kwargs)
    second_lens = np.fft.fftshift(np.fft.ifft2(np.fft.ifftshift(filtered)))
    u_obtained = propagator.propagate(second_lens, wavelength, pix, z)
    u_obtained = u_obtained / np.sqrt(np.sum(np.abs(u_obtained) ** 2))
    return u_obtained


def lee_method(u_target, ky, kx, wavelength, pix, z, **kwargs):
    dmd_complex = complex_to_dmd_pixels(u_target, ky, kx, wavelength, pix, z)
    obtained_norm_wave = system_4f(dmd_complex, wavelength, pix, z, **kwargs)
    return obtained_norm_wave


def plot_complex_wave(complex_wave):
    plt.subplot(121)
    plt.imshow(np.abs(complex_wave) ** 2, cmap='gray')
    plt.colorbar()
    plt.subplot(122)
    plt.imshow(np.angle(complex_wave), cmap='gray')
    plt.colorbar()
    plt.show()


full_name_int = "./bluelakes.jpg"
full_name_phase = "./mountains.jpg"
u_to_proc = make_complex_field_from_image(full_name_int, full_name_phase)

wavelength_exp = 0.532 * 10 ** (-6)
pixel = 7.56 * 10 ** (-6)
period = 8
spatial_freq = 1 / period
z_prop = 1.5
obtained = lee_method(u_to_proc, spatial_freq/10 ** 9, spatial_freq, wavelength_exp, pixel, z_prop, resolution=8)

plot_complex_wave(obtained)
