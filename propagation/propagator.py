import numpy as np
from scipy import constants
from scipy import signal


def gauss_beam_creation(ny, nx, pix, rho):
    size_x = nx * pix
    size_y = ny * pix
    x = np.linspace(-size_x / 2, size_x / 2 - size_x / nx, nx)
    y = np.linspace(-size_y / 2, size_y / 2 - size_y / ny, ny)
    ii = np.arange(ny)[:, np.newaxis]
    jj = np.arange(nx)
    gauss = np.exp(-1j * (y[ii] ** 2 + x[jj] ** 2) / rho ** 2)
    return gauss


def angular_spectrum(u, wavelength, pix, z):
    wavenumber = 2 * np.pi / wavelength
    ny, nx = u.shape
    size_y, size_x = ny * pix, nx * pix
    fy = np.linspace(- ny / 2 / size_y, ny / 2 / size_y - 1 / size_y, ny)
    fx = np.linspace(- nx / 2 / size_x, nx / 2 / size_x - 1 / size_x, nx)
    ii = np.arange(ny)[:, np.newaxis]
    jj = np.arange(nx)
    h = np.where(np.sqrt(fx[jj] ** 2 + fy[ii] ** 2) < 1 / wavelength,
                 np.exp(1j * wavenumber * z * np.sqrt(1 - wavelength ** 2 * (fy[ii] ** 2 + fx[jj] ** 2))),
                 0)
    u_interm = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(u)))
    u_interm *= h
    u_diff_z = np.fft.fftshift(
        np.fft.ifft2(np.fft.ifftshift(u_interm))
    )
    return u_diff_z


def convolution(u, wavelength, pix, z):
    ny, nx = u.shape
    wavenumber = 2 * np.pi / wavelength
    size_y, size_x = ny * pix, nx * pix
    u_zp = np.pad(u, [(ny // 2, ny // 2), (nx // 2, nx // 2)], mode='constant', constant_values=(0 + 0j))
    size_y_zp = size_y * 2
    size_x_zp = size_x * 2
    fft_zp = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(u_zp)))
    ny_zp, nx_zp = u_zp.shape
    x = np.linspace(-size_x_zp / 2, size_x_zp / 2 - pix, nx_zp)
    y = np.linspace(-size_y_zp / 2, size_y_zp / 2 - pix, ny_zp)
    ii = np.arange(ny_zp)[:, np.newaxis]
    jj = np.arange(nx_zp)
    # Fresnel approximation
    h_c = np.exp(1j * z * wavenumber) / 1j / wavelength / z * np.exp(1j * wavenumber / 2 / z *
                                                                     (x[jj] ** 2 + y[ii] ** 2))
    # Diffraction integral
    # h_c = np.exp(1j * wavenumber * np.sqrt(x[jj] ** 2 + y[ii] ** 2 + z ** 2)) / 1j / wavelength / np.sqrt(
    #     x[jj] ** 2 + y[ii] ** 2 + z ** 2)
    fft_h = np.fft.fftshift(np.fft.fft2(np.fft.ifftshift(h_c)))
    u_interm = fft_zp * fft_h
    u_diff_z = pix ** 2 * np.fft.fftshift(
        np.fft.ifft2(np.fft.ifftshift(u_interm))
    )
    u_cut = u_diff_z[ny // 2: ny // 2 + ny, nx // 2: nx // 2 + nx]

    return u_cut


def propagate(u, wavelength, pix, z):
    ny, nx = u.shape
    max_n = np.maximum(ny, nx)
    size_max = max_n * pix
    nu_0 = constants.c * np.abs(z) / size_max / (size_max / max_n)
    if z == 0:
        return u
    nu = constants.c / wavelength
    if nu < nu_0:
        print(z, "conv")
        return convolution(u, wavelength, pix, z)
    print(z, "as")
    return angular_spectrum(u, wavelength, pix, z)


# usage: u_propagated = propagate(u_initial, wavelength, pixel_size, propagation_distance)
